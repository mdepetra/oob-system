#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <inttypes.h>
#include "list.h"
#include "util.h"

/**
 * PROGETTO DI LABORATORIO DI SISTEMI OPERATIVI - Sessione Invernale 2020
 * @file supervisor.c
 * @brief Il supervisor e' il server centrale attiva k server che stimano il secret per piu' client.
 * @author Mirko De Petra
 */

#define SERVERPATH	"./server"
#define FILENAME	"server"

static volatile sig_atomic_t end = 0;

// Lista dei client con le relative stime
list_client* clients = NULL;

// Gestore del segnale SIGINT
static void sighandlerSI(int signum) {
	int notused;

    //La funzione alarm() ritorna 0 se non e' atteso nessun allarme.
    if ((notused=alarm(1))==0) return;
    
	end=1;
}

// Gestore del segnale SIGALRM
static void sighandlerSA(int signum) {
	// Quando il supervisor riceve un segnale SIGINT, stampare sul suo stderr una tabella che mostri le sue stime correnti
	// Fare qui la stampa, non è thread safe
	// SOLUZIONE ALTERNATIVA: usare una seconda variabile static volatile sig_atomic_t e controllarne lo status nel ciclo while in cui si leggono le stime dalla pipe
	stampa_lista(stderr,clients);
}

// Procedura che stampa il messaggio d'uso
static void usage(const char*argv0) {
    fprintf(stderr, "use: %s #servers-da-lanciare\n", argv0);
}

// Procedura che controlla la correttezza degli argomenti
static void checkargs(int argc, char* argv[]) {
    if (argc != 2) {
		usage(argv[0]);
		_exit(EXIT_FAILURE);
    }
	// Il supervisor riceve sulla riga di comando un valore intero k: il numero di server da lanciare
    if ((int)strtol(argv[1], NULL, 10)<2) {
		fprintf(stderr, "Il numero di server da lanciare deve essere almeno 2.\n\n");
		usage(argv[0]);
		_exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[]) {
	// Si controlla la correttezza degli argomenti ricevuti
	checkargs(argc, argv);

	int notused;

	// Struttura per i segnali
	struct sigaction saI,saA;
    // Reset delle strutture
    memset (&saI, 0, sizeof(saI)); 
    memset (&saA, 0, sizeof(saA)); 
    // Registrazione dei gestori
    saI.sa_handler = sighandlerSI;
    saA.sa_handler = sighandlerSA;
    // Installazione degli handler
    SYSCALL(notused,sigaction(SIGINT,   &saI, NULL), "sigaction");
    SYSCALL(notused,sigaction(SIGALRM,  &saA, NULL), "sigaction");
	
	// Conversione dell'argomento ricevuto
    int k = (int)strtol(argv[1], NULL, 10);
    
	// Il supervisor stampa sul suo stdout un messaggio “SUPERVISOR STARTING k”
	fprintf(stdout,"SUPERVISOR STARTING %d\n", k);
	fflush(stdout);
	
	int i;
	// Il supervisor lancia (come processi distinti) i k server
	// Mantiente con ciascun server una connessione tramite pipe anonime con cui potra ricevere le stime dei secret dei client da parte dei server.
	int pid[k];
	char* server_id = NULL;
	char* fd_pipe = NULL;

	CHECKNULL(server_id, malloc(sizeof(int)), "malloc");
	memset(server_id,'0',sizeof(int));
	CHECKNULL(fd_pipe, malloc(sizeof(int)), "malloc");
	memset(fd_pipe,'0',sizeof(int));
	int fd[2]; //per la pipe
	
	SYSCALL(notused, pipe(fd), "pipe");

	for (i=0; i<k; i++){
		// Creazione dei figli
		SYSCALL(pid[i], fork(), "fork");
		if (pid[i] == 0) {
			/* figli */
			close(fd[0]); // chiusura della lettura
			snprintf(server_id, sizeof(int), "%d", i+1);
			snprintf(fd_pipe, sizeof(int), "%d", fd[1]);
			SYSCALL(notused, execl(SERVERPATH, FILENAME, server_id, fd_pipe, (char*)NULL),"execl");
		}
	}
	/* padre */
	free(server_id);
	free(fd_pipe);
	
	close(fd[1]); // chiusura della scrittura

	messaggio_stima m;
	
	while(!end)  {
    	if((notused=(read(fd[0], &m, sizeof (messaggio_stima))))==-1 && errno == EINTR) { 
    		continue;
    	}
		// Si inserisci la stima ricevuta nella lista
    	inserisci_stima(&clients, m.id_client, m.secret_s, m.i_server);
		// Per ogni nuova stima ricevuta, il supervisor stampa su stdout un messaggio “SUPERVISOR ESTIMATE s FOR id FROM i”, 
		fprintf(stdout, "SUPERVISOR ESTIMATE %d FOR %" PRIx64 " FROM %d\n", m.secret_s, m.id_client, m.i_server);
		fflush(NULL);
	}
	
	/**
	 * Se invece riceve un “doppio Ctrl-C”, ovvero un secondo segnale SIGINT a distanza di meno di un secondo dal precedente, 
	 * il supervisor stampa la stessa tabella su stdout
	 */
	stampa_lista(stdout,clients);

	// Il supervisor manda il segnale SIGTERM hai figli server
	for (i=0;i<k;i++)
		SYSCALL(notused, kill(pid[i], SIGTERM), "kill child");
	 
	// Il supervisor stampa il messaggio “SUPERVISOR EXITING”.
	fprintf(stdout,"SUPERVISOR EXITING\n");
	fflush(stdout);
	
	// Il supervisor termina.
	return 0;
}
