#!/bin/bash

# PROGETTO DI LABORATORIO DI SISTEMI OPERATIVI - Sessione Invernale 2020
# Author: Mirko De Petra

# se il numero di argomenti non e' giusto si stampa il comando d'uso (nomescript supervisor.log client.log)
if [ $# -ne 2 ]; then
    echo use: $(basename $0) supervisor.log client.log
    exit -1
fi

# si controlla se i file esistono
SUPERVISORFILE=$1
if [ ! -f $SUPERVISORFILE ]; then
    echo "il file $SUPERVISORFILE non esiste o non e' un file regolare"
    exit 1
fi

CLIENTFILE=$2
if [ ! -f $CLIENTFILE ]; then
    echo "il file $CLIENTFILE non esiste o non e' un file regolare"
    exit 1
fi

MAXERRORE=25 # errore massimo che si puo' commettere nella stima del secret
erroremedio=0 # errore medio nel calcolo del secret
stimecorrette=0 # numero di stime corrette
stimeerrate=0 # numero di stime errate
stimetotali=0 # numero di stime totali

i=0
# si estrae la tabella delle stime dal file supervisor.log
while read line; do
    read -r -a elem <<< "$line" # separazione degli elementi della riga line
    if [[ $line == *"BASED ON"* ]]; then
        ID[$i]=${elem[4]}
        STIME[$i]=${elem[2]}
        (( i++ ))
    fi 
done <$SUPERVISORFILE

# il numero di stime totali e' uguale al numero di elementi nell'array
stimetotali=${#STIME[@]}

i=0
# per ogni ID nell'array si recupera il secret dal file client.log usando il comando grep
for client in "${ID[@]}"; do
    line=$(grep ${client} ${CLIENTFILE}) 
    read -r -a elem <<< "$line" # separazione degli elementi della riga line
    secret=${elem[3]}
    # stampa Client ID_client Secret Secret_Client Stima Stima_Supervisor
    echo -e "Client ${client}\t\tSecret ${secret}\tStima ${STIME[$i]}"
    
    # calcolo dell'errore assoluto di rappresentazione 
    eass=$((STIME[$i] - secret)) 

    if (($eass < 0)); then
		eass=$(($eass*-1))
	fi

    # si verifica che il secret stimato sia con errore entro MAXERRORE unita' rispetto al valore del secret vero
	if (($eass <= $MAXERRORE)); then
		(( stimecorrette+=1 ))
	else
		(( stimeerrate+=1 ))
	fi
    (( i++ ))

    # l'errore totale e' dato dagli errori effettuati in ogni singola stima
	(( erroremedio+=$eass ))
done

# calcolo dell'errore medio arrotondato per difetto
(( erroremedio/=stimetotali ))

echo "#--------------------#"
echo "Statistiche"
echo "Secret stimati con successo: ${stimecorrette}/${stimetotali}" 
echo "Secret stimati con errore: ${stimeerrate}/${stimetotali}" 
echo "Errore medio di stima: ${erroremedio}"
