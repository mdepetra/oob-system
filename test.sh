#!/bin/bash

# PROGETTO DI LABORATORIO DI SISTEMI OPERATIVI - Sessione Invernale 2020
# Author: Mirko De Petra

# Lancia 8 server
echo script $0 
echo "Lancio del supervisor con 8 server"
./supervisor 8 >> supervisor.log &

# Attesa di 2 secondi
sleep 2

# I client andranno lanciati a coppie, e con un attesa di 1 secondo fra ogni coppia.
echo "Lancio di 20 client"
for ((i = 0; i < 10; i++)); do
    ./client 5 8 20 >> client.log &
    ./client 5 8 20 >> client.log &
    sleep 1
done

# Attesa di 60 secondi
# Nel frattempo, invio di un SIGINT al supervisor ogni 10 secondi
for ((i = 0; i < 6; i++)); do
    pkill -SIGINT supervisor
    sleep 10
    tmp=$(((i+1)*10))
    echo "$tmp secondi"
done

# Invio di un doppio SIGINT
pkill -SIGINT supervisor
pkill -SIGINT supervisor

echo "supervisor e i server sono stati terminati"

# Lancio dello script misura sui risultati raccolti
echo "Lancio di misura.sh"
bash ./misura.sh supervisor.log client.log
