/**
 * PROGETTO DI LABORATORIO DI SISTEMI OPERATIVI - Sessione Invernale 2020
 * @file util.h
 * @brief Header file contenente delle utility
 * @author Mirko De Petra
 */
#if !defined(UTIL_H)
#define UTIL_H

#include <sys/types.h> 
#include <stdint.h> 
#include <inttypes.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#define SYSCALL(r,c,e) \
    if((r=c)==-1) { perror(e);exit(errno); }
#define CHECKNULL(r,c,e) \
    if ((r=c)==NULL) { perror(e);exit(errno); }

#define MAXBACKLOG   32
#define PREFIXSOCKNAME "OOB-server-"
#define MAXSOCKETNAME sizeof(PREFIXSOCKNAME)+2

/**
 * Messaggio di stima
 * Contiene:
 *  secret_s è il secret stimato;
 *  i_server è il numero del server che ha fatto la stima;
 *  id_client è l'ID del client.
 */
typedef struct _messaggio_stima {
    int secret_s; // Secret stimato
    int i_server; // Numero del server che ha stimato il secret
    uint64_t id_client; // ID del client
} messaggio_stima;

// Presa dal corso di laboratorio di Sistemi Operativi
static inline int readn(long fd, void *buf, size_t size) {
    size_t left = size;
    int r;
    char *bufptr = (char*)buf;
    while(left>0) {
	if ((r=read((int)fd ,bufptr,left)) == -1) {
	    if (errno == EINTR) continue;
	    return -1;
	}
	if (r == 0) return 0; 
        left    -= r;
	bufptr  += r;
    }
    return size;
}

// Presa dal corso di laboratorio di Sistemi Operativi
static inline int writen(long fd, void *buf, size_t size) {
    size_t left = size;
    int r;
    char *bufptr = (char*)buf;
    while(left>0) {
        if ((r=write((int)fd ,bufptr,left)) == -1) {
            if (errno == EINTR) continue;
            return -1;
        }
        if (r == 0) return 0;  
            left    -= r;
        bufptr  += r;
    }
    return 1;
}

#endif /* UTIL_H */
