#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#include "util.h"

/**
 * PROGETTO DI LABORATORIO DI SISTEMI OPERATIVI - Sessione Invernale 2020
 * @file server.c
 * @brief Il server riceve messaggi da piu' client. Osserve il  tempo trascorso fra i messaggi consecuti di uno stesso client e fa una stima del secret.
 * @author Mirko De Petra
 */

static volatile sig_atomic_t end=0;

// Gestore del segnale SIGTERM
static void sighandler(int signum) {
	end = 1;
}

// Procedura che stampa il messaggio d'uso
static void usage(const char*argv0);

// Procedura che controlla la correttezza degli argomenti
static void checkargs(int argc, char* argv[]);

// Procedura di pulizia
void cleanup();

// Gestisce il singolo client
void *threadF(void *arg);

// Crea i thread
void spawn_thread (long arg);

int i, fd;
char sockname[MAXSOCKETNAME+1];

int main(int argc, char *argv[]) {
	// Il server viene lanciato dal supervisor
	
	// Si controlla la correttezza degli argomenti ricevuti
	checkargs(argc, argv);
	
	int notused;
	
	// Strutture per i segnali
	struct sigaction saSI,saST;
	
    // Reset delle strutture
    memset (&saSI, 0, sizeof(saSI));   
    memset (&saST, 0, sizeof(saST)); 
    
    // Registrazione dei gestori
    saSI.sa_handler = SIG_IGN;
    saST.sa_handler = sighandler;
   
	// Installazione dei signal handler 
    SYSCALL(notused,sigaction(SIGINT,   &saSI, NULL), "sigaction");
    SYSCALL(notused,sigaction(SIGTERM,   &saST, NULL), "sigaction");
    
    // Si cancella il socket file se esiste
    cleanup();
    // Se qualcosa va storto
    atexit(cleanup);
	
	// Conversione degli argomenti ricevuti
    i = (int)strtol(argv[1], NULL, 10);
    fd = (int)strtol(argv[2], NULL, 10);

	// Creazione dell'indirizzo della socket (si usi come indirizzo la stringa “OOB-server-i”, in cui i e il suo numero progressivo fra 1 e k). 
	snprintf(sockname, sizeof sockname, "%s%d", PREFIXSOCKNAME, i);
	
	// Il server apre una socket nel dominio AF_UNIX
	int listenfd;
    SYSCALL(listenfd, socket(AF_UNIX, SOCK_STREAM, 0), "socket");
    
    // Si setta l'indirizzo 
    struct sockaddr_un serv_addr;
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;    
    strncpy(serv_addr.sun_path, sockname, strlen(sockname)+1);
    
    // Si assegna l'indirizzo al socket 
    SYSCALL(notused, bind(listenfd, (struct sockaddr*)&serv_addr,sizeof(serv_addr)), "bind");
    
    // Si setta il socket in modalita' passiva e si definisce il numero massimo di connessioni pendenti
    SYSCALL(notused, listen(listenfd, MAXBACKLOG), "listen");
    
	// Il server stampa sul suo stdout un messaggio nel formato “SERVER i ACTIVE”.
	fprintf(stdout, "SERVER %d ACTIVE\n", i);
	
	int fd_c;

	while (!end){
		if((fd_c=accept(listenfd, (struct sockaddr*)NULL, 0))==-1 && errno == EINTR) { 
    		continue;
    	}
    	// Per ogni connessione, il server stampa un messaggio “SERVER i CONNECT FROM CLIENT”.
		fprintf(stdout, "SERVER %d CONNECT FROM CLIENT.\n", i);
		fflush(NULL);
		
        spawn_thread(fd_c);
    }

    close(listenfd);
	
	return 0;
}

/**
 * Procedura che stampa il messaggio d'uso.
 */
static void usage(const char*argv0) {
    fprintf(stderr, "use: %s numeroServer fdPipe \n", argv0);
}

/**
 * Procedura che controlla la correttezza degli argomenti.
 * 
 * @param argc -- numero di argomenti ricevuti
 * @param argv -- array degli argomenti ricevuti
 * 
 */
static void checkargs(int argc, char* argv[]) {
    if (argc != 3) {
		usage(argv[0]);
		_exit(EXIT_FAILURE);
    }
    if ((int)strtol(argv[1], NULL, 10)<1) {
    	fprintf(stderr, "Il numero progressivo che identifica il server deve essere compreso fra 1 e k \n\n");
		usage(argv[0]);
		_exit(EXIT_FAILURE);
    }
}

/**
 * Procedura di pulizia
 */
void cleanup() {
    unlink(sockname);
}

/**
 * Ogni thread gestisce un client
 */
void *threadF(void *arg) {
    long connfd = (long)arg;
	uint64_t id_c = 0, id_nbo_c;
	struct timespec now; 
    long diff;
	int stima = -1;
	long time_now;
	long time_prec = 0;
	int sendMsg = 0; // Vale 0 (se si è ricevuto un solo messaggio dal client)
	int n;
	
    do {
		SYSCALL(n, readn(connfd, &id_nbo_c, sizeof(id_nbo_c)), "readn");
	
		if (n==0) 
			break;
			
		// Si prende il tempo attuale
		clock_gettime(CLOCK_REALTIME, &now); 
		
		// Trasformazione del tempo attuale in millisecondi
		time_now = now.tv_sec * 1000; 
		time_now = (time_now + (now.tv_nsec * 1e-6));
	
		// Si ottiene l'id (da NBO a HBO)
		long int hbo_1 = ntohl(id_nbo_c>>32);
		long int hbo_2 = ntohl(id_nbo_c);
	
		id_c = hbo_1 | (hbo_2<<32);
		
		/**
		 * Per ogni messaggio ricevuto sulla socket, stampa su stdout un messaggio “SERVER i INCOMING FROM id @ t”,
		 * in cui id e' l'ID del client che ha inviato il messaggio, e t e' il tempo di arrivo (espresso come un numero in MILLISECONDI).
		 */
		fprintf(stdout, "SERVER %d INCOMING FROM %" PRIx64 " @ %ld\n", i, id_c, time_now);
		
		/**
		 * Si osserva il momento di arrivo dei messaggi da parte del client, e in particolare il tempo trascorso fra messaggi consecutivi. 
		 * Si stima il secret di ogni client
		 */
		diff = abs(time_now - time_prec);
		if (stima == -1) {
			stima = diff;
		} else {
			if (diff < stima) {
				stima = diff;
			}
			sendMsg = 1; // è arrivato piu' di un messaggio 
		}
		time_prec = time_now;
		printf("stima %d, ", stima);
    } while(1);

	// Il server stampa su stdout un messaggio “SERVER i CLOSING id ESTIMATE s”, in cui s e il valore stimato da questo client per il secret del client id.
	fprintf(stdout, "SERVER %d CLOSING %" PRIx64 " ESTIMATE %d\n", i, id_c, stima);
	fflush(stdout);
	
	if (sendMsg) {
		/**
		 * Se sono arrivati almeno 2 messaggi, è stata fatta una stima del secret.
		 * Il server informa il supervisor di qual e' la sua migliore stima per il valore del secret di id (la stessa stampata nel messaggio).
		 * Il server invia un messaggio che contiene l'ID del client e la sua stima del secret.
		 */
		
		// Creazione del messaggio
		messaggio_stima m;
		m.secret_s = stima;
		m.i_server = i;
		m.id_client = id_c;
	
		// Invio del messaggio
		int notused;
		SYSCALL(notused, writen(fd, &m, sizeof (messaggio_stima)), "writen pipe");
	}
	
    close(connfd);
    pthread_exit(NULL);
}

/**
 * Crea i thread
 */
void spawn_thread(long connfd) {
    pthread_attr_t thattr;
    pthread_t thid;

    if (pthread_attr_init(&thattr) != 0) {
		fprintf(stderr, "pthread_attr_init FALLITA\n");
		close(connfd);
		return;
    }
    
    // Si setta il thread in modalità detached
    if (pthread_attr_setdetachstate(&thattr,PTHREAD_CREATE_DETACHED) != 0) {
		fprintf(stderr, "pthread_attr_setdetachstate FALLITA\n");
		pthread_attr_destroy(&thattr);
		close(connfd);
		return;
    }
    
    if (pthread_create(&thid, &thattr, threadF, (void*)connfd) != 0) {
		fprintf(stderr, "pthread_create FALLITA");
		pthread_attr_destroy(&thattr);
		close(connfd);
		return;
    }
}
