# ---------------------------------------------------------------------------
# PROGETTO DI LABORATORIO DI SISTEMI OPERATIVI - Sessione Invernale 2020
# Author: Mirko De Petra
# ---------------------------------------------------------------------------

CC =  gcc
CFLAGS =  -std=c99 -Wall -g
INCLUDES	= -I.
LIBS = -pthread

TARGETS	= server \
		client \
		supervisor

.PHONY: all clean test

all		: server client supervisor

supervisor: supervisor.c list.o
	$(CC) $(CFLAGS) $^ -o $@ $(INCLUDES) 

server:	server.c
	$(CC) $(CFLAGS) $^ -o $@ $(INCLUDES) $(LIBS)

client: client.c
	$(CC) $(CFLAGS) $(INCLUDES) $^ -o $@

lista.o: lista.c
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<

clean		: 
	rm -f $(TARGETS) *.o *~ ./OOB* *.log

test:
	bash ./test.sh
