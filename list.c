#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include "list.h"

/**
 * PROGETTO DI LABORATORIO DI SISTEMI OPERATIVI - Sessione Invernale 2020
 * @file list.c
 * @brief File di implementazione di una lista di client
 * @author Mirko De Petra
 */

/**
 * Funzione di utilità che inserisce in testa una nuova stima del secret per il client id_c.
 * La stima ha valore secr ed è stata ottenuta dal server id_s.
 * Se è la prima stima ricevuta per il client id_c, si crea l'elemento client e viene inserito in testa alla lista dei client.
 * La lista deve essere passata con un doppio * per poterne modificare la testa.
 */
void inserisci_stima(list_client** lista, uint64_t id_c, int secr, int id_s) {
    // Il supervisor riceve ovviamente stime da piu' server per lo stesso client. 
	// Utilizzare le diverse stime arrivate fino a un dato momento per determinare il valore piu' probabile del secret “vero” (chiamiamo questo valore Sid).

    list_client* curr = *lista;
    
    while(curr != NULL && curr->id_client != id_c) {
        curr = curr->next;
    }
    
    // Se curr è diventato NULL vuol dire che la lista era vuota oppure che id_c non era in lista.
    if(curr == NULL) {
        /* Creazione del nuovo elemento da aggiungere alla lista */
        list_client* new_client = malloc(sizeof(list_client));
        
        // Si aggiunge l'id_client
        new_client->id_client = id_c;
        
        /* Creazione del nuovo elemento da aggiungere alla lista delle stime*/
        list_stima* new_stima = malloc(sizeof(list_stima));
        new_stima->secret_s = secr;
        new_stima->i_server = id_s;
        new_stima->next = NULL;
    
        // Si aggiunge la prima stima
        new_client->stime = new_stima;
        
        // Si inizializza a 1 il numero di server
        new_client->n = 1;
        
        // Si inizializza a secr il secret stimato fino a questo momento
        new_client->s = secr;
        
        new_client->next = *lista; 
        // Il nuovo elemento dovrà puntare alla vecchia testa, che in questo momento si trova in *lista.
        *lista = new_client;
    }
    else {
        list_stima** l_stime = &(curr->stime);
        
        (curr->n)++;
        // Creazione del nuovo elemento di lista
        list_stima* new_stima = malloc(sizeof(list_stima));
        new_stima->secret_s = secr;
        new_stima->i_server = id_s;
        // Il nuovo elemento dovrà puntare alla vecchia testa, che in questo momento si trova in *lista.
        new_stima->next = *l_stime;
    
        // Inserimento del nuovo elemento (modifica della testa)
        *l_stime = new_stima;
        int secr_t = curr->s;
    
        // Stima del secret piu' probabile
        curr->s = secr_t<secr ? secr_t : secr;
    }
}

/**
 * Funzione di utilità che dealloca la memoria occupata dalla lista passata come parametro.
 */
void distruggi_lista(list_client* lista) {
    while(lista != NULL) {
        list_stima* curr_s = lista->stime;
        while(curr_s != NULL) {
            list_stima* tmp_s = curr_s->next;
            free(curr_s);
            curr_s = tmp_s;
        }
        list_client* tmp_c = lista->next;
        free(lista);
        lista = tmp_c;
    }
}

/**
 * Funzione di utilità per stampare una lista.
 * Stampa una tabella che mostra le stime correnti in cui ciascuna riga ha il formato
 *			“SUPERVISOR ESTIMATE Si d FOR id BASED ON n” 
 */
void stampa_lista(FILE* s,list_client* lista) {
    list_client* curr = lista;
    
    while(curr != NULL) {
        fprintf(s,"SUPERVISOR ESTIMATE %d FOR %" PRIx64 " BASED ON %d\n", curr->s, curr->id_client, curr->n);
        fflush(s);
        curr = curr->next;
    }
}
