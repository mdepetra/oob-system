/**
 * PROGETTO DI LABORATORIO DI SISTEMI OPERATIVI - Sessione Invernale 2020
 * @file list.h
 * @brief Header file contenente la dichiarazione di una lista di client
 * linkata singolarmente, con operazioni di inserimento di stime per ciascun client,
 * e altre utility.
 * @author Mirko De Petra
 */

#ifndef list_h
#define list_h

#include <stdint.h>
#include <inttypes.h>

/*
 * Elemento di lista di stime
 * Contiene:
 *  secret_s e' il secret stimato;
 *  i_server e' il numero del server che ha fatto la stima;
 *  next e' il puntatore all'elemento successivo.
 */
typedef struct _elemento_di_lista_stime {
    int secret_s; // Secret stimato
    int i_server; // Numero del server che ha stimato il secret
    struct _elemento_di_lista_stime* next; // Puntatore al prossimo elemento
} list_stima;

/*
 * Elemento di lista di client
 * Contiene:
 *  id_client e' l'id del client;
 *  stime e' la lista delle stime ricevute;
 *  s e' la stima del secret;
 *  n e' il numero di server che hanno fornito stime per id fino a quel momento;
 *  next e' il puntatore all'elemento successivo.
 */
typedef struct _client {
	 uint64_t id_client;
	 list_stima* stime;
	 int s; 
	 int n; 
	 struct _client* next;
} list_client;

/**
 * Funzione di utilità che inserisce in testa una nuova stima del secret per il client id_c.
 * La stima ha valore secr ed è stata ottenuta dal server id_s.
 * Se è la prima stima ricevuta per il client id_c, si crea l'elemento client e viene inserito in testa alla lista dei client.
 * La lista deve essere passata con un doppio * per poterne modificare la testa.
 */
void inserisci_stima(list_client** lista, uint64_t id_c, int secr, int id_s);

/**
 * Funzione di utilità che dealloca la memoria occupata dalla lista passata come parametro.
 */
void distruggi_lista(list_client* lista);

/**
 * Funzione di utilità per stampare una lista.
 * Stampa una tabella che mostra le stime correnti in cui ciascuna riga ha il formato
 *			“SUPERVISOR ESTIMATE Si d FOR id BASED ON n” 
 */
void stampa_lista(FILE* s,list_client* list);

#endif /* list_h */
