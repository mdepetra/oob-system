#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h> 
#include <unistd.h>
#include <time.h>
#include <arpa/inet.h>
#include "util.h"

/**
 * PROGETTO DI LABORATORIO DI SISTEMI OPERATIVI - Sessione Invernale 2020
 * @file client.c
 * @brief I client possiedono un codice segreto (secret) e vogliono comunicarlo a un server centrale, senza pero' trasmetterlo.
 * @author Mirko De Petra
 */

#define MINSECRET   1		/* minimo valore che può assumere il secret */
#define MAXSECRET   3000	/* massimo valore che può assumere il secret */

// Procedura che stampa il messaggio d'uso
static void usage(const char*argv0);

// Procedura che controlla la correttezza degli argomenti
static void checkargs(int argc, char* argv[]);

// Procedura che seleziona i server a cui connettersi
void selectserver (int *servers, int p, int k);

// Funzione che apre una socket per il server identificato da server_id
int opensocket(int server_id);

int main(int argc, char *argv[]) {
	// Si controlla la correttezza degli argomenti ricevuti
	checkargs(argc, argv);
	
	// Conversione degli argomenti ricevuti
    int p = (int)strtol(argv[1], NULL, 10);
    int k = (int)strtol(argv[2], NULL, 10);
    int w = (int)strtol(argv[3], NULL, 10);
	
	uint64_t id, id_nbo;
	int i, secret;
	int servers[p], sock_fds[p];
	long int p1, p2;
	long int nbo_1, nbo_2;
	
	// Inizializzazione del generatore di numeri random con il PID
	srand(getpid());
	
	// Il client genera pseudo-casualmente il secret
	// Il secret e' costituito da un numero fra 1 e 3000, e' rappresentato da un numero decimale
	secret = (rand() % (MAXSECRET - MINSECRET + 1))+ MINSECRET;
	
	// Il client genera pseudo-casualmente l'ID Unico
	// L'ID unico e' un intero a 64 bit, e' rappresentato da un numero esadecimale (senza prefissi particolari)
	p1 = rand();
	p2 = rand();
	id = p1 | (p2<<32);
	
	// L'ID unico in Network Byte Order
	nbo_1 = htonl(p1);
	nbo_2 = htonl(p2);
	id_nbo = ((nbo_1)<<32) | nbo_2;
	
	// Il client stampa sul proprio stdout un messaggio nel formato “CLIENT id SECRET secret”
	fprintf(stdout, "CLIENT %" PRIx64 " SECRET %d\n", id, secret);

	// Il client sceglie casualmente p interi distinti compresi fra 1 e k
	selectserver(servers,p,k);
	
	// Il client si collega (tramite socket) ai p server corrispondenti agli interi scelti
	for (i=0;i<p;i++)
		sock_fds[i] = opensocket(servers[i]);
	
	// Creazione di una variabile per l'intervallo di tempo da aspettare tra un messaggio e il successivo.
	struct timespec t;
	t.tv_sec = (int)(secret/1000);
    t.tv_nsec = (secret % 1000) * 1e6;
	
	int server_scelto, notused, sockfd;
    
	// Il client inizia un ciclo per inviare complessivamente w messaggi (in totale, non per ciascun server).
	for (i=0;i<w;i++){
		// Sceglie casualmente un server fra i p a cui e' collegato.
		server_scelto = (rand() % p);
		sockfd = sock_fds[server_scelto];
		// Al server scelto invia sulla socket corrispondente un messaggio contenente il proprio ID unico;
		SYSCALL(notused, writen(sockfd, &id_nbo, sizeof(uint64_t)), "writen");
		// Attende secret millisecondi prima di ripetere (si puo' usare la  system call nanosleep(2) per l'attesa).
		nanosleep(&t, NULL);
	}
	
	// Chiusura delle socket aperte
	for (i=0;i<p;i++)
		close(sock_fds[i]);

	// Una volta completato il proprio compito, il client stampa un messaggio “CLIENT id DONE”.
	fprintf(stdout, "CLIENT %" PRIx64 " DONE\n", id);

	// Il client termina
	return 0;
}

/**
 * Procedura che stampa il messaggio d'uso.
 */
static void usage(const char*argv0) {
    fprintf(stderr, "use: %s #servers-da-scegliere #servers #num-max-messaggi-da-inviare\n", argv0);
}

/**
 * Procedura che controlla la correttezza degli argomenti.
 * 
 * @param argc -- numero di argomenti ricevuti
 * @param argv -- array degli argomenti ricevuti
 * 
 */
static void checkargs(int argc, char* argv[]) {
    if (argc != 4) {
		usage(argv[0]);
		_exit(EXIT_FAILURE);
    }
    /** 
	 * Il client riceve sulla riga di comando tre valori interi:
	 *	p -> numero di server a cui il client deve connettersi.
	 *	k -> dovra' essere lo stesso passato al supervisor, che controlla quanti server sono attivi sul sistema. 
	 *	w -> numero di messaggi da inviare.
	 *	con 1 ≤ p < k e w > 3p
	 */
    int p = (int)strtol(argv[1], NULL, 10);
    if (p<1) {
		fprintf(stderr, "Il numero di server da scegliere deve essere almeno 1.\n\n");
		usage(argv[0]);
		_exit(EXIT_FAILURE);
    }
    if ((int)strtol(argv[2], NULL, 10)<=p) {
		fprintf(stderr, "Il numero di server attivi nel sistema deve essere maggiore del numero di server scelti.\n\n");
		usage(argv[0]);
		_exit(EXIT_FAILURE);
    }
    if ((int)strtol(argv[3], NULL, 10)<=3*p) {
		fprintf(stderr, "Il numero di messaggi da inviare deve essere maggiore di 3*p.\n\n");
		usage(argv[0]);
		_exit(EXIT_FAILURE);
    }
}

/**
 * Procedura che seleziona i server a cui connettersi.
 * Usa un algoritmo basato su "Fisher–Yates shuffle" per generare una permutazione di un array di interi.
 * 
 * @param servers -- l'array in cui inserire i server selezionati
 * @param p -- il numero di server da scegliere
 * @param k -- il numero di server attivi nel sistema 
 * 
 */
void selectserver (int *servers, int p, int k) {
	int i, j, t;
	int temp_a[k];
	
	// Inizializzazione dell'array temp_a con valori da 1 a k.
	for (i=0;i<k;i++)
		temp_a[i]=i+1;
	
	for (i=0;i<p;i++){
		// Selezione di un numero random tra i e k (inclusi).
		j = (rand() % (k - i))+ i;
		// Scambio del valore in posizione i con quello in posizione j
		t = temp_a[i];
		temp_a[i] = temp_a[j];
		temp_a[j] = t;
	}
	
	// Trasferimento dei primi p valori in temp_a ad servers
	for (i=0;i<p;i++)
		servers[i]=temp_a[i];
}

/**
 * Funzione che apre una socket per il server identificato da server_id.
 * 
 * @param server_id -- numero del server a cui connettersi
 * 
 */
int opensocket(int server_id){
	// Praparazione del sockname
    char sockname[MAXSOCKETNAME+1];
    snprintf(sockname, sizeof sockname, "%s%d", PREFIXSOCKNAME, server_id);

	struct sockaddr_un serv_addr;
    int sockfd;
	SYSCALL(sockfd, socket(AF_UNIX, SOCK_STREAM, 0), "socket");
	
    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sun_family = AF_UNIX;    
    strncpy(serv_addr.sun_path, sockname, strlen(sockname)+1);

	// Uso la funzione connect() per stabilire una connessione con il server server_num
    int notused;
    SYSCALL(notused, connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)), "connect");
	
    return sockfd;
}
